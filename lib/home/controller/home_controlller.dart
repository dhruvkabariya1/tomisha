import 'package:get/get.dart';

class HomeController extends GetxController{
  RxInt selectedTab = 0.obs;
  void onTapTab(int idx){
    selectedTab.value = idx;
  }
}