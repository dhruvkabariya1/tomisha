import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:tomisha/home/view/mobile/mobile_default_view.dart';

import '../web/web_default_view.dart';

class DefaultView extends StatelessWidget {
  const DefaultView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: BottomRoundDesign(),
      child: Container(
        alignment: Alignment.topCenter,
        height: 660.h,
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color(0xffEBF4FF), Color(0xffE6FFFA)])),
        child: kIsWeb
            ? WebDefaultView()
            : MobileDefaultView()
      ),
    );
  }
}

class BottomRoundDesign extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0, size.height);

    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height - 25);
    path.quadraticBezierTo(size.width - (size.width / 5), size.height - 35,
        size.width, size.height - 50);

    path.lineTo(size.width, size.height - 50);
    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
