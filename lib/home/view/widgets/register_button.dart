import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class RegisterButton extends StatelessWidget {
  const RegisterButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding:kIsWeb ?null  :EdgeInsets.symmetric(vertical: 12.h, horizontal: 90.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12.r)),
          gradient: const LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Color(0xff3182CE), Color(0xff319795)])),
      child: Text(
        "Kostenlos Registrieren",
        style: GoogleFonts.lato(
            fontWeight: FontWeight.w700,
            fontSize:kIsWeb?14 :14.sp,
            color: Color(0xffE6FFFA)),
      ),
    );
  }
}
