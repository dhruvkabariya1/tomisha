import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:tomisha/home/view/mobile/mobile_part_1.dart';
import 'package:tomisha/home/view/web/web_part_1.dart';

import '../../controller/home_controlller.dart';

class Part1 extends GetView<HomeController> {
   Part1({Key? key}) : super(key: key);

  final List<String> showTitiles = ["Erstellen dein Lebenslauf","Erstellen dein Lebenslauf","Erstellen dein Lebenslauf"];
  

  @override
  Widget build(BuildContext context) {
  

    return Container(

      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child:kIsWeb ? WebPart1(): MobilePart1()
    );
  }
}
