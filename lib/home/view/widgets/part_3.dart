import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tomisha/home/view/mobile/mobile_part_3.dart';
import 'package:tomisha/home/view/web/web_part_3.dart';

import '../../controller/home_controlller.dart';

class Part3 extends GetView<HomeController> {
  Part3({Key? key}) : super(key: key);

  final List<String> showTitiles = [
    "Erstellen dein Lebenslauf",
    "Wähle deinen\nneuen Mitarbeiter aus",
    "Vermittlung nach\nProvision oder\nStundenlohn"
  ];
  final List<String> showImage = [
    "assets/mobile/undraw_personal_file_222m.png",
    "assets/mobile/undraw_swipe_profiles1_i6mr.png",
    "assets/mobile/undraw_business_deal_cpi9.png"
  ];

  final List<String> showImageWeb = [
    "mobile/undraw_personal_file_222m@2x.png",
    "mobile/undraw_swipe_profiles1_i6mr@2x.png",
    "mobile/undraw_business_deal_cpi9@2x.png"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.topCenter,
        child: kIsWeb ? WebPart3() : MobilePart3());
  }
}
