import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class RegistrationBar extends StatelessWidget {
  const RegistrationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
                width: MediaQuery.of(context).size.width,
                height: 67.h,
                
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                   BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0, -3.h),
                        blurRadius: 6,
                        spreadRadius: 2.r)
                  ],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(12.r),
                      topRight: Radius.circular(12.r)),
                ),
                alignment: Alignment.center,
                child: Container(

                  padding:
                      EdgeInsets.symmetric(vertical: 12.h, horizontal: 90.w),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12.r)),
                      gradient: const LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Color(0xff3182CE), Color(0xff319795)])),
                  child: Text(
                    "Kostenlos Registrieren",
                    style: GoogleFonts.lato(
                        fontWeight: FontWeight.w700,
                        fontSize: 14.sp,
                        color: Color(0xffE6FFFA)),
                  ),
                ),
              );
  }
}