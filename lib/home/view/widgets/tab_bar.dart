import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../../controller/home_controlller.dart';
import 'tabs.dart';

class Tabs extends GetView<HomeController> {
  Tabs({Key? key}) : super(key: key);

  final List<String> showDetailText = [
    "Drei einfache Schritte\nzu deinem neuen Job",
    "Drei einfache Schritte\nzu deinem neuen Mitarbeiter",
    "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      
      child: Obx(
        () => Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 35.h,bottom: 55.h),
              height: 40.h,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: [
                  TabTitle(
                    title: "Arbeitnehmer",
                    borderRadius:
                        BorderRadius.horizontal(left: Radius.circular(12.r)),
                    margin: EdgeInsets.only(left: 20.w),
                    selected: controller.selectedTab.value == 0,
                    onSelect: () {
                      controller.onTapTab(0);
                    },
                  ),
                  TabTitle(
                    title: "Arbeitgeber",
                    selected: controller.selectedTab.value == 1,
                    onSelect: () {
                      controller.onTapTab(1);
                    },
                  ),
                  TabTitle(
                    title: "Temporärbüro",
                    borderRadius:
                        BorderRadius.horizontal(right: Radius.circular(12.r)),
                    margin: EdgeInsets.only(right: 20.w),
                    selected: controller.selectedTab.value == 2,
                    onSelect: () {
                      controller.onTapTab(2);
                    },
                  ),
                ],
              ),
            ),
           
            Text(
              showDetailText[controller.selectedTab.value],
              textAlign: TextAlign.center,
              style:
                  GoogleFonts.lato(fontSize:kIsWeb ?  40.sp : 21.sp, color: Color(0xff4A5568)),
            )
          ],
        ),
      ),
    );
  }
}
