import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginBar extends StatelessWidget {
  const LoginBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 5.h,
          decoration: const BoxDecoration(
           
            gradient: LinearGradient(
                colors: [Color(0xff319795), Color(0xff3182CE)],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight),
          ),
         constraints: BoxConstraints(minHeight: 5.h,maxHeight: 5.h), 
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 67.h,
          padding: EdgeInsets.symmetric(horizontal: 17.w),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0, 1.h),
                  blurRadius: 4,
                  spreadRadius: 1.r)
            ],
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(12.r),
                bottomRight: Radius.circular(12.r)),
          ),
          alignment: Alignment.centerRight,
          child: Text(
            "Login",
            style: GoogleFonts.lato(
              fontWeight: FontWeight.bold,
              color: const Color(0xff319795),
              fontSize: 14.sp,
            ),
          ),
        ),
      ],
    );
  }
}
