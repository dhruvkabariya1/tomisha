import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class TabTitle extends StatelessWidget {
  const TabTitle({Key? key, this.borderRadius, required this.title, this.margin, required this.selected, required this.onSelect}) : super(key: key);
  final BorderRadius? borderRadius;
  final String title;
  final EdgeInsetsGeometry? margin;
  final bool selected;
  final VoidCallback onSelect; 

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onSelect,
      child: Container(
        width:160.w,
        height:40.h,
        margin: kIsWeb?  null:margin,
        decoration: BoxDecoration(
          color:selected ? Color(0xff81E6D9): Colors.white,
           //#319795
          borderRadius: borderRadius,
          border:!selected? Border.all(color: const Color(0xffcbd5e0),width: 1) : null,
        ),
        alignment: Alignment.center,
        padding: EdgeInsets.zero,
        child: Text(title,style: GoogleFonts.lato(
          color:selected ? Color(0xffE6FFFA): Color(0xff319795),
          fontWeight: FontWeight.w600,
          fontSize: 14.sp
        )),
      ),
    );
  }
}