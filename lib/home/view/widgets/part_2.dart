import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:tomisha/home/view/mobile/mobile_part_2.dart';

import '../../controller/home_controlller.dart';
import '../web/web_part_2.dart';

class Part2 extends GetView<HomeController> {
  Part2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
        ClipPath(
          clipper:kIsWeb?BottomRoundDesignPart2Web() : BottomRoundDesignPart2Mobile(),
          child: Container(
              height: 370.h,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Color(0xffEBF4FF), Color(0xffE6FFFA)])),
              child: kIsWeb ? WebPart2() : MobilePart2()),
        
    );
  }
}

class BottomRoundDesignPart2Mobile extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, 0);

    path.quadraticBezierTo(size.width / 4, 50, size.width / 2, 30);
    path.quadraticBezierTo(3 / 4 * size.width, 5, size.width, 10);

    path.lineTo(size.width, size.height - 50);

    path.quadraticBezierTo(
        3 / 4 * size.width, size.height - 10, size.width / 2, size.height - 40);
    path.quadraticBezierTo(size.width / 4, size.height - 40, 0, size.height);

    path.lineTo(0, size.height);
    path.lineTo(0, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}

class BottomRoundDesignPart2Web extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, 0);
    path.lineTo(0, 5);

    path.quadraticBezierTo(0, 0, size.width / 10, 0);
    path.quadraticBezierTo(size.width*(2/ 10) , 10, size.width*(4/ 10), 20);
    path.quadraticBezierTo(size.width *(5/ 10) , 25, size.width*(8/ 10), 0);
    path.quadraticBezierTo( size.width-50, 5, size.width, 50);

    path.lineTo(size.width, size.height - 70);

    path.quadraticBezierTo(
        (8 / 10 )* size.width, size.height, size.width / 2, size.height - 22);
    path.quadraticBezierTo(( 3/10 )*size.width , size.height-25, 0, size.height);

    path.lineTo(0, size.height);
    path.lineTo(0, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
