import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../widgets/register_button.dart';


class WebDefaultView extends StatelessWidget {
  const WebDefaultView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
              margin: EdgeInsets.only(top: 133.h,bottom: 70.h),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 374.w,
                    ),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Deine Job\nwebsite",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.lato(
                                fontWeight: FontWeight.bold,
                                  fontSize: 42, color: const Color(0xff2D3748)),
                            ),
                          ),
                          SizedBox(
                            height: 53.w,
                          ),
                          SizedBox(
                              height: 40.h, width: 320.w, child: RegisterButton())
                        ],
                      ),
                    SizedBox(
                      width: 115.w,
                    ),
                     CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 227.r,
                        child: ClipOval(
                          child: Image.asset(
                            "mobile/undraw_agreement_aajr@2x.png",
                               
                            alignment: Alignment.center,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                  
                  ],
                ),
            ); 
      
  }
}