import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../../controller/home_controlller.dart';

class WebPart1 extends GetView<HomeController> {
  WebPart1({Key? key}) : super(key: key);

  final List<String> showTitiles = [
    "Erstellen dein Lebenslauf",
    "Erstellen dein Lebenslauf",
    "Erstellen dein Lebenslauf"
  ];

  @override
  Widget build(BuildContext context) {
    return
        Container(
          margin: EdgeInsets.only(bottom: 200.h, top: 100.h),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.alphabetic,
                children: [
                  Text(
                    "1.",
                    style: GoogleFonts.lato(
                        fontSize: 130.sp, color: Color(0xff718096)),
                  ),
                  SizedBox(
                    width: 23.h,
                  ),
                  Obx(() => Text(
                        showTitiles[controller.selectedTab.value],
                        style: GoogleFonts.lato(
                            color: Color(0xff718096), fontSize: 30.sp),
                      ))
                ],
              ),
              SizedBox(
                width: 60.h,
              ),
              SizedBox(
                height: 250.h,
                width: 384.w,
                child: Image.asset(
                  "mobile/undraw_Profile_data_re_v81r@2x.png",
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                width: 180.h,
              ),
            ],
          ),
        );
  }
}
