import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controller/home_controlller.dart';

class WebPart3 extends GetView<HomeController> {
  WebPart3({Key? key}) : super(key: key);
  final GlobalKey text3 = GlobalKey();

  final List<String> showTitiles = [
    "Mit nur einem Klick\nbewerben",
    "Wähle deinen\nneuen Mitarbeiter aus",
    "Vermittlung nach\nProvision oder\nStundenlohn"
  ];
  final List<String> showImageWeb = [
    "mobile/undraw_personal_file_222m@2x.png",
    "mobile/undraw_swipe_profiles1_i6mr@2x.png",
    "mobile/undraw_business_deal_cpi9@2x.png"
  ];

  @override
  Widget build(BuildContext context) {
  //  Offset position=  (text3.currentContext?.findRenderObject() as RenderBox).localToGlobal(Offset.zero);

    return Obx(
      () => Stack(
      alignment: Alignment.center,
     
        children: [
          Positioned(
            top: 180.h,
            left:480.w,
            
            child: Container(
              height: 300.h,
              width: 300.h,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 240, 245, 250),
                shape: BoxShape.circle
              ),
              child: SizedBox(
                height: 300.w,
              width: 300.w,
              ),
            ),
          ),
          
          Container(
            margin: EdgeInsets.only(top: 100.h, bottom: 234.h),
                  height:345.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      key: text3,
                      "3.",
                      style: GoogleFonts.lato(
                          fontSize: 130.sp, color: Color(0xff718096)),
                    ),
                    SizedBox(
                      width: 23.h,
                    ),
                    Text(
                      showTitiles[controller.selectedTab.value],
                      style:
                          GoogleFonts.lato(color: Color(0xff718096), fontSize: 30.sp),
                    )
                  ],
                ),
                SizedBox(
                  width: 28.w,
                ),
                Image.asset(
                  showImageWeb[controller.selectedTab.value],
                  fit: BoxFit.contain,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
