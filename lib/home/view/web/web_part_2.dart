import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter/foundation.dart' show kIsWeb;

import '../../controller/home_controlller.dart';

class WebPart2 extends GetView<HomeController> {
  WebPart2({Key? key}) : super(key: key);

  final List<String> showTitiles = [
    "Erstellen dein Lebenslauf",
    "Erstellen ein Jobinserat",
    "Erhalte Vermittlungs-\nangebot von Arbeitgeber"
  ];
  final List<String> showImageWeb = [
    "mobile/undraw_task_31wc@2x.png",
    "mobile/undraw_about_me_wa29@2x.png",
    "mobile/undraw_job_offers_kw5d@2x.png"
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => 
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 227.h,
                width: 324.w,
                child: Image.asset(
                  showImageWeb[controller.selectedTab.value],
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                width: 112.w,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Text(
                        "2.",
                        style: GoogleFonts.lato(
                            fontSize: 130.sp, color: Color(0xff718096)),
                      ),
                      SizedBox(
                        width: 23.w,
                      ),
                      Text(
                        showTitiles[controller.selectedTab.value],
                        style: GoogleFonts.lato(
                            color: Color(0xff718096), fontSize: 30.sp),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
    );
    
    
  }
}
