import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controller/home_controlller.dart';

class MobilePart3 extends GetView<HomeController> {
  MobilePart3({Key? key}) : super(key: key);

  final List<String> showTitiles = [
    "Erstellen dein Lebenslauf",
    "Wähle deinen\nneuen Mitarbeiter aus",
    "Vermittlung nach\nProvision oder\nStundenlohn"
  ];
  final List<String> showImage = [
    "assets/mobile/undraw_personal_file_222m.png",
    "assets/mobile/undraw_swipe_profiles1_i6mr.png",
    "assets/mobile/undraw_business_deal_cpi9.png"
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        margin: EdgeInsets.only(bottom: 114.h),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              textBaseline: TextBaseline.alphabetic,
              children: [
                Text(
                  "3.",
                  style: GoogleFonts.lato(
                      fontSize: 130.sp, color: Color(0xff718096)),
                ),
                SizedBox(
                  width: 23.h,
                ),
                Text(
                  showTitiles[controller.selectedTab.value],
                  style:
                      GoogleFonts.lato(color: Color(0xff718096), fontSize: 16.sp),
                )
              ],
            ),
            SizedBox(
              height: 210.h,
              width: 281.w,
              child: Image.asset(
                showImage[controller.selectedTab.value],
                fit: BoxFit.contain,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
