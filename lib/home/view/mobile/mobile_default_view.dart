import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../widgets/register_button.dart';


class MobileDefaultView extends StatelessWidget {
  const MobileDefaultView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 113.h,
                    margin: EdgeInsets.only(top: 90.h),
                    alignment: Alignment.center,
                    child: Text(
                      "Deine Job\nwebsite",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lato(
                        fontWeight: FontWeight.bold,
                          fontSize: 42, color: const Color(0xff2D3748)),
                    ),
                  ),
                  SizedBox(
                    height: 404.h,
                    width: 360.w,
                    child: Image.asset(
                      "assets/mobile/undraw_agreement_aajr.png",
                      alignment: Alignment.center,
                      fit: BoxFit.cover,
                    ),
                  )
                ],
              );
           
  }
}

