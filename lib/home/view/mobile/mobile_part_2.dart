import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter/foundation.dart' show kIsWeb;

import '../../controller/home_controlller.dart';

class MobilePart2 extends GetView<HomeController> {
  MobilePart2({Key? key}) : super(key: key);

  final List<String> showTitiles = [
    "Erstellen dein Lebenslauf",
    "Wähle deinen\nneuen Mitarbeiter aus",
    "Erhalte Vermittlungs-\nangebot von Arbeitgeber"
  ];
  final List<String> showImage = [
    "assets/mobile/undraw_task_31wc.png",
    "assets/mobile/undraw_about_me_wa29.png",
    "assets/mobile/undraw_job_offers_kw5d.png"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 25.h,bottom: 40.h),
      child: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: [
                Text(
                  "2.",
                  style: GoogleFonts.lato(
                      fontSize: 130.sp, color: Color(0xff718096)),
                ),
                SizedBox(
                  width: 23.h,
                ),
                Text(
                  showTitiles[controller.selectedTab.value],
                  style:
                      GoogleFonts.lato(color: Color(0xff718096), fontSize: 16.sp),
                )
              ],
            ),
            Image.asset(
              showImage[controller.selectedTab.value],
              fit: BoxFit.contain,
            )
          ],
        ),
      ),
    );
  }
}
