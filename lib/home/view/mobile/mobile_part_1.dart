import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../../controller/home_controlller.dart';

class MobilePart1 extends GetView<HomeController> {
   MobilePart1({Key? key}) : super(key: key);

  final List<String> showTitiles = ["Erstellen dein Lebenslauf","Erstellen dein Lebenslauf","Erstellen dein Lebenslauf"];
  

  @override
  Widget build(BuildContext context) {
  

    return Container(
      height: 200.h,
      margin: EdgeInsets.only(top: 20.h),
      
      child: Stack(
        fit: StackFit.loose,
          alignment: Alignment.bottomLeft,
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Container(
                margin: EdgeInsets.only(right: 20.w),
                width: 220.w,
                
                child: Image.asset(
                "assets/mobile/undraw_Profile_data_re_v81r.png",
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: [
                Text(
                  "1.",
                  style: GoogleFonts.lato(
                      fontSize: 130.sp, color: Color(0xff718096)),
                ),
                SizedBox(
                  width: 23.h,
                ),
                Obx(()=>Text(
                  showTitiles[controller.selectedTab.value],
                  style:
                      GoogleFonts.lato(color: Color(0xff718096), fontSize: 16.sp),
                ))
              ],
            ),],),
    );
        
      
  }
}
