import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'widgets/default_view.dart';
import 'widgets/login_bar.dart';
import 'widgets/part_1.dart';
import 'widgets/part_2.dart';
import 'widgets/part_3.dart';
import 'widgets/registration_bar.dart';
import 'widgets/tab_bar.dart';

class HomeMobile extends StatelessWidget {
  const HomeMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: [
            ListView(
              children:  [  
                DefaultView(),
                Tabs(),
                Stack(
                  children: [
                Positioned(
            top: 550.h,
            left:-100.w,
            
            child: Container(
              height: 300.h,
              width: 300.h,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 240, 245, 250),
                shape: BoxShape.circle
              ),
              child: SizedBox(
                height: 300.w,
              width: 300.w,
              ),
            ),
          ),
                    Column(
                      children: [
                        Part1(),
                        Part2(),
                        Part3(),
                      ],
                    ),
               
                  ],
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [LoginBar(), RegistrationBar()],
            ),
          ],
        ),
      ),
    );
  }
}


