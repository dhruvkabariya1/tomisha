import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/responsive.dart';
import '../controller/home_controlller.dart';
import 'home_mobile.dart';
import 'home_web.dart';



class Home extends StatelessWidget {
   Home({Key? key}) : super(key: key);

final HomeController controller =Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return const Responsive(
      mobileMode: HomeMobile(),
      webMode: HomeWeb(),
    );    
  }
}