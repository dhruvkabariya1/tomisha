import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha/home/view/widgets/login_bar.dart';

import 'widgets/default_view.dart';
import 'widgets/part_1.dart';
import 'widgets/part_2.dart';
import 'widgets/part_3.dart';
import 'widgets/tab_bar.dart';

class HomeWeb extends StatelessWidget {
  const HomeWeb({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          ListView(
            children: [
              DefaultView(),
              Tabs(),
              Stack(
                children: [
                  Column(
                    children: [
                      Part1(),
                      Part2(),
                      Part3(),
                    ],
                  ),
                  Positioned(
                    left: 470.w,
                    top: 300.h,
                    child: SizedBox(
                      height: 390.h,
                      width: 580.w,
                      child: Image.asset(
                        "web/Gruppe_1821@2x.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Positioned(
                      left: 580.w,
                      top: 880.h,
                      child: SizedBox(
                          height: 230.h,
                          width: 530.w,
                          child: Image.asset(
                            "web/Gruppe_1822@2x.png",
                            fit: BoxFit.contain,
                          ))),
                ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [LoginBar()],
          ),
        ],
      ),
    );
  }
}
