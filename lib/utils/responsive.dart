import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  const Responsive({Key? key, required this.mobileMode, required this.webMode})
      : super(key: key);

  final Widget mobileMode;
  final Widget webMode;

  @override
  Widget build(BuildContext context){
    return kIsWeb ?webMode : mobileMode;
  }
}
